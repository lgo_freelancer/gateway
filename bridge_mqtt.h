#ifndef BRIDGE_MQTT_H
#define BRIDGE_MQTT_H
#include <stdint.h>
#include "config_structs.h"
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <Client.h>

void bridge_init();
uint32_t bridge_get_date(void);
Client * bridge_get_client(void);
void mqtt_publish_node_data(Sensor_Data * node_data, uint16_t node_id, Gateway_Cfg * gwc, Adafruit_MQTT_Client &mqtt_client);
uint32_t mqtt_last_ct(void);
void mqtt_ping(Adafruit_MQTT_Client &mqtt_client);

#endif