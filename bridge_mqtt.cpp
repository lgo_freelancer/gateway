#include "bridge_mqtt.h"
#include <Bridge.h>
#include <BridgeClient.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <Process.h>
#include <ArduinoJson.h>
#include "flash_simple_api.h"

BridgeClient client;
Process date;
uint32_t mqtt_last_connection_time = 0;

void bridge_init(void){
     Bridge.begin();
}

uint32_t bridge_get_date(void){
      if (!date.running()) {
    date.begin("date");
    date.addParameter("+%s");
    date.run();
  }
  date.setTimeout(3000);
  uint32_t ret = date.parseInt();
  return ret;
}

Client * bridge_get_client(void){
  return &client;
}

void mqtt_publish_node_data(Sensor_Data * node_data, uint16_t node_id, Gateway_Cfg * gwc, Adafruit_MQTT_Client &mqtt_client) {

  int8_t ret;
  if (!mqtt_client.connected()) {
    uint8_t attempts = 2;
    while (((ret = mqtt_client.connect()) != 0) && (attempts--)) { // connect will return 0 for connected
      Serial.println(mqtt_client.connectErrorString(ret));
      Serial.println(F("Retrying MQTT connection in 5 seconds..."));
      mqtt_client.disconnect();
      delay(5000);  // wait 5 seconds
    }
  }
  mqtt_last_connection_time = millis(); /* Update last connection time */

  Node_Gateway_Cfg node_config;
  gateway_node_config_read(node_id, &node_config);
  char topic_base[200];
  sprintf(topic_base, "/%s/%s/", gwc->gateway_location, gwc->gateway_name);

  StaticJsonBuffer<200> jsonBuffer;
 
  JsonArray& root = jsonBuffer.createArray();

  JsonObject& root_0 = root.createNestedObject();
  root_0["nodeName"] = node_config.node_name;
  root_0["rssi"] = node_data->rssi;
  root_0["bat"] = node_data->battery_level;
  root_0["time"] = node_data->timestamp;

  JsonObject& root_1 = root.createNestedObject();
  root_1["tipoSensor"] = node_config.sensor_type_p1;
  root_1["nameSensor"] = node_config.sensor_name_p1;
  root_1["value"] = node_data->p1;

  JsonObject& root_2 = root.createNestedObject(); 
  root_2["tipoSensor"] = node_config.sensor_type_p2;
  root_2["nameSensor"] = node_config.sensor_name_p2;
  root_2["value"] = node_data->p2;

  JsonObject& root_3 = root.createNestedObject();
  root_3["tipoSensor"] = node_config.sensor_type_p3;
  root_3["nameSensor"] = node_config.sensor_name_p3;
  root_3["value"] = node_data->p3;

  JsonObject& root_4 = root.createNestedObject();
  root_4["tipoSensor"] = node_config.sensor_type_p4;
  root_4["nameSensor"] = node_config.sensor_name_p4;
  root_4["value"] = node_data->p4;

  char json[200];
  root.printTo(json,200);
  Adafruit_MQTT_Publish data = Adafruit_MQTT_Publish(&mqtt_client, topic_base);
  if ( !data.publish(json) ) {
    /* Failed */
  }

}

uint32_t mqtt_last_ct(void){
    return mqtt_last_connection_time;
}

void mqtt_ping(Adafruit_MQTT_Client &mqtt_client){
  mqtt_client.ping();
  mqtt_last_connection_time = millis();
}
