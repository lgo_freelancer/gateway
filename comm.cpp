#include "comm.h"
#include <Arduino.h>
#include <RHReliableDatagram.h>
#include <RHEncryptedDriver.h>
#include <SPI.h>
#include "config.h"
#include "flash_simple_api.h"
#include "config_structs.h"
#include "bridge_mqtt.h"
#include <string.h>

#if defined(ARDUINO_ARCH_SAMD)
extern Uart Serial;
#else
extern HardwareSerial Serial;
#endif

#if USE_RF95 == 1
#include <RH_RF95.h>
RH_RF95 driver(RF95_CSS_PIN, 2);

#else

#include <RH_NRF24.h>
RH_NRF24 driver(8, 9);

#endif

RHReliableDatagram manager(driver, 34);


static uint8_t send_signal(uint8_t val, uint8_t to) {
  bool res = manager.sendtoWait(&val, 1, to);
  if (res == true) {
    return 0;
  } else {
    return 1;
  }
}

uint8_t rf_init(void) {
  bool res;
  if ((res = manager.init()) == true) {
    return 0;
  } else {
    return 1;
  }
}

void rf_set_address(uint8_t address) {
  manager.setThisAddress(address);
}

void rf_set_frequency(float freq) {
#if USE_RF95 == 1
  driver.setFrequency(freq);
#else

#endif
}

void rf_set_power(int8_t pwr) {
#if USE_RF95 == 1
  driver.setTxPower(pwr);
#else

#endif
}

int16_t rf_rssi(void) {
  return driver.lastRssi();
}
void rf_sleep(void) {
  driver.sleep();
}
void rf_set_timeout(uint32_t to) {
  manager.setTimeout(to);
}


enum rf_res_type listen_for_node_data(Sensor_Data * sdata, uint8_t * from, Gateway_Cfg * gwc) { //Adafruit_MQTT_Client &mqtt_client) {
  /* Wait for data */
  uint8_t rx_buffer[300];
  uint8_t len = sizeof(rx_buffer);

  if ( manager.recvfromAck(rx_buffer, &len, from) ) {
    /* Recebe os daods pela serial */
    Serial.print(F("Packet Received "));
    Serial.print(F("From: ")); Serial.print(*from);
    Serial.print(F(" Len: ")); Serial.println(len);
    Serial.println("");

    if ((len == 1) && (rx_buffer[0] == 0xAA)) { /* Config request */

      /* Pedido de configuração */
      Serial.println(F("Config req."));
      /* Check Validator */
      /* Verifica o validador */
      Node_Gateway_Cfg node_config;
      uint8_t ret;
      ret = gateway_node_config_read(*from, &node_config);
      if (ret == CFG_DOESNT_EXIST) {
        Serial.println(F("There is no config. file for this node"));/* Não a configura. arquivo para este Nó */
        return CONFIG_REQUEST; /* No data for this node */ /* Não a dados para este Nó */
      }
      if (node_config.validador != 0) {
        Serial.println(F("Config. Already Sent"));/* Configuração ja foi enviada */
        return CONFIG_REQUEST; /* Config has been sent already. */
      }

      Node_RF_Cfg node_config_rf;
      node_config_rf.collection_time = node_config.collection_time;
      node_config_rf.gateway_id = gwc->gateway_id;
      node_config_rf.node_id = *from;
      node_config_rf.power = node_config.power;
      node_config_rf.epoch = bridge_get_date();

      if (manager.sendtoWait((uint8_t*)&node_config_rf, sizeof(Node_RF_Cfg), *from)) {
        /* Data sent. Mark Validator as True */
        node_config.validador = 1;
        /* Store in flash */
        Serial.println(F("Config Data sent to the node"));
        gateway_node_config_update(&node_config);
      } else {
        Serial.println(F("Config Data ERROR"));
      }
      return CONFIG_REQUEST;
    } else if ((rx_buffer[0] == 0xBB)) { /* AUTH packet */

      Serial.println(F("AUTH Packet"));
      Node_Gateway_Cfg node_config;
      uint8_t ret;
      ret = gateway_node_config_read(*from, &node_config);
      if (ret == CFG_DOESNT_EXIST) {
        Serial.println(F("There is no config. file for this node"));/* Não a configura. arquivo para este Nó */
        return AUTH_REQUEST; /* No data for this node */ /* Não a dados para este Nó */
      }
      int is_not_the_same = memcmp(node_config.node_device_id, &rx_buffer[1], 12);

      if (is_not_the_same) {
        Serial.println(F("Node Device ID mismatch"));
        return AUTH_REQUEST;
      }
      Serial.println();
      /* Send back a packet with the device id and the epoch */
      uint32_t now;
      now = bridge_get_date();
      uint8_t packet[18];
      packet[0] = 0xBB;
      memcpy(&packet[1], node_config.node_device_id, 12);
      memcpy(&packet[13], (uint8_t*)&now, 4);

      if (manager.sendtoWait(packet, sizeof(packet), *from)) {
        Serial.println(F("AUTH packet sent"));
      }
      return AUTH_REQUEST;
    } else if ((len == 1) && (rx_buffer[0] == 0xA6)) {
      /* Sensor data */
      Serial.println(F("Sensor packet"));
      
      uint8_t size_to_rx = sizeof(Sensor_Data);
      
      uint32_t elapsed_t = 0;
      uint8_t segment_from;
      uint16_t buffer_index = 0;
      uint8_t res = 0;

      manager.setTimeout(1000);
      
      res = send_signal(0xA6, *from);
      if (res != 0) {
        return NOTHING;
      }

      
      uint16_t timeout = 5000;
      uint32_t st = millis();
      
      while (elapsed_t < timeout) {
        len = sizeof(rx_buffer);
        if (manager.recvfromAck(&rx_buffer[buffer_index], &len, &segment_from)) {
          if (segment_from == *from) {
            buffer_index += len;
          }

          //Serial.print(len); Serial.print(" "); Serial.print(segment_from); Serial.print(" "); Serial.println(buffer_index);

          if (buffer_index == size_to_rx) {
            uint8_t i = 0;
            while (buffer_index--) {
              Serial.print(rx_buffer[i++], HEX); Serial.print(" ");
            }
            Serial.println();
            break;
          }
          if (buffer_index > size_to_rx) {
            return NOTHING;
          }

        }


        elapsed_t = millis() - st;
      }

      if (elapsed_t >= timeout) {
        Serial.println(F("Timeout"));
        return NOTHING;
      }
      memcpy((uint8_t*)sdata, rx_buffer, sizeof(Sensor_Data));
      Serial.print(F("Thermopar ")); Serial.println(sdata->thermopar_data);
      Serial.print(F("Battery ")); Serial.println(sdata->battery_level);
      Serial.print(F("RSSI ")); Serial.println(sdata->rssi);
      Serial.print(F("TS ")); Serial.println(sdata->timestamp);
      return SENSOR_DATA;
      /* Publish */
      // publish_node_data(node_data_ptr, from, mqtt_client);
    } else {
      Serial.print(F("Unknown packet received"));
      return NOTHING;
    }
  }
  return NOTHING;
}

// /* Retuns 0 if AUTH packet is received, 1 otherwise */
// uint8_t rf_ask_for_auth(Node_Internal_Cfg * cfg)
// {
//   const uint8_t gateway_id = cfg->gateway_id;
//   uint8_t packet[14];
//   uint8_t device_id[13];
//   device_id_get(device_id);

//   packet[0] = 0xBB;                  /* Start Marker */
//   memcpy(&packet[1], device_id, 12); /* device_id, 12 bytes */

//   uint8_t rx_packet[25];
//   uint8_t len = sizeof(rx_packet);
//   uint8_t from;

//   Serial.println(F("Sending AUTH request..."));
//   if (manager.sendtoWait(packet, sizeof(packet), gateway_id))
//   {
//     if (manager.recvfromAckTimeout(rx_packet, &len, 2000, &from))
//     {
//       if (from == gateway_id)
//       {

//         /* Check if the returned device_id is the same as mine - 12 bytes */
//         const int res = memcmp(&rx_packet[1], device_id, 12);

//         if (res)
//         {
//           return 1;
//         }

//         /* Get the time in epoch format */
//         uint32_t now;
//         memcpy((uint8_t *)&now, &rx_packet[13], 4);
//         rtc_set(now);

//         Serial.println(F("AUTH OK"));
//         return 0;
//       }
//     }
//   }
//   else
//   {
//     return 1;
//   }
//   return 1;
// }
