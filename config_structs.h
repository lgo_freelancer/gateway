#ifndef CONFIG_STRUCTS_H
#define CONFIG_STRUCTS_H
#include <stdint.h>

typedef struct node_internal_cfg_str{
  uint32_t epoch;
  uint16_t collection_time;
  uint8_t node_id;
  uint8_t gateway_id;
  uint8_t power;
  uint8_t crypto_key[16];
} Node_Internal_Cfg;

typedef struct node_rf_cfg_str{
  uint8_t node_id;
  uint16_t collection_time;
  uint8_t gateway_id;
  uint8_t power;
  uint32_t epoch;
} Node_RF_Cfg;

typedef struct sensor_data_str{
  int16_t rssi;
  uint8_t battery_level;
  double thermopar_data;
  char timestamp[20];
  char p1[20];
  char p2[20];
  char p3[20];
  char p4[20];
} Sensor_Data;
  
typedef struct node_gtw_cfg_str{
  uint16_t collection_time;
  uint8_t gateway_id;
  uint8_t validador;  
  uint8_t node_id;
  uint8_t node_device_id[13];
  uint8_t power;
  uint8_t crypto_key[16];
  char node_location[40];
  char node_name[40];
  char sensor_type_p1[40];
  char sensor_name_p1[40];
  char sensor_type_p2[40];
  char sensor_name_p2[40];
  char sensor_type_p3[40];
  char sensor_name_p3[40];
  char sensor_type_p4[40];
  char sensor_name_p4[40];
} Node_Gateway_Cfg;

typedef struct gateway_cfg_str{
  uint16_t port_mqtt;
  uint8_t gateway_id;
  uint8_t power;
  uint8_t crypto_key[16];
  char gateway_location[40];
  char gateway_name[40];
  char user_mqtt[40];
  char pass_mqtt[40];
  char host_mqtt[40];
} Gateway_Cfg;


#endif
