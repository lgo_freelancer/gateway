#include "print_helpers.h"
#include "config_structs.h"
#include <Arduino.h>
#include <Wire.h>
#include "device_id.h"

#ifdef __AVR_ATmega2560__
extern HardwareSerial Serial;
#endif

static void print_EEPROM_MAC_and_firmware(int deviceaddress, byte eeaddress);

static void pr_qt(){
  Serial.print("\"");
}
static void pr_sep(){
  Serial.print(",");
}
static void pr_field(const char * field){
  Serial.print("\"");
  Serial.print(field);
  Serial.print("\":");
}
void printer_gw_config(Gateway_Cfg * cfg){
	Serial.print("{ ");

	pr_field("id");
	Serial.print(cfg->gateway_id); pr_sep();
	pr_field("name");
	pr_qt(); Serial.print(cfg->gateway_name); pr_qt(); pr_sep();
	pr_field("location");
	pr_qt(); Serial.print(cfg->gateway_location); pr_qt(); pr_sep();
	pr_field("mqtt_host");
	pr_qt(); Serial.print(cfg->host_mqtt); pr_qt(); pr_sep();
	pr_field("mqtt_user");
	pr_qt(); Serial.print(cfg->user_mqtt); pr_qt(); pr_sep();
	pr_field("mqtt_pass");
	pr_qt(); Serial.print(cfg->pass_mqtt); pr_qt(); pr_sep();
	pr_field("mqtt_port");
	Serial.print(cfg->port_mqtt); pr_sep();
	pr_field("power");
	Serial.print(cfg->power); pr_sep();
	pr_field("key");
	pr_qt();
	for(uint8_t z=0;z<16;z++){
	  Serial.print((char)(cfg->crypto_key)[z]); 
	}
	pr_qt();
	Serial.print("}");
}

void printer_node_config(Node_Gateway_Cfg * cfg){
    Serial.print("{ ");

  pr_field("id");
  Serial.print(cfg->node_id); pr_sep();
  pr_field("device_id"); 
  pr_qt(); Serial.print((char *)cfg->node_device_id);  pr_qt(); pr_sep();
  pr_field("name");
  pr_qt(); Serial.print(cfg->node_name); pr_qt(); pr_sep();
  pr_field("location");
  pr_qt(); Serial.print(cfg->node_location); pr_qt(); pr_sep();
  pr_field("gateway_id");
  Serial.print(cfg->gateway_id); pr_sep();
  pr_field("validador");
  Serial.print(cfg->validador); pr_sep();
  pr_field("collection_time");
  Serial.print(cfg->collection_time); pr_sep();
  pr_field("power");
  Serial.print(cfg->power); pr_sep();
  pr_field("key");
  pr_qt();
  for(uint8_t z=0;z<16;z++){
    Serial.print((char)(cfg->crypto_key)[z]); 
  } 
  pr_qt();  pr_sep();
  pr_field("sensor_name_p1");
  pr_qt(); Serial.print(cfg->sensor_name_p1); pr_qt(); pr_sep();
  pr_field("sensor_type_p1");
  pr_qt(); Serial.print(cfg->sensor_type_p1); pr_qt(); pr_sep();
  
  pr_field("sensor_name_p2");
  pr_qt(); Serial.print(cfg->sensor_name_p2); pr_qt(); pr_sep();
  pr_field("sensor_type_p2");
  pr_qt(); Serial.print(cfg->sensor_type_p2); pr_qt(); pr_sep();
  
  pr_field("sensor_name_p3");
  pr_qt(); Serial.print(cfg->sensor_name_p3); pr_qt(); pr_sep();
  pr_field("sensor_type_p3");
  pr_qt(); Serial.print(cfg->sensor_type_p3); pr_qt(); pr_sep();
  
  pr_field("sensor_name_p4");
  pr_qt(); Serial.print(cfg->sensor_name_p4); pr_qt(); pr_sep();
  pr_field("sensor_type_p4");
  pr_qt(); Serial.print(cfg->sensor_type_p4); pr_qt();
  
  Serial.print("}");
}


void printer_device_id(){
  Wire.begin();
  print_EEPROM_MAC_and_firmware(0x50, 0xF8);  //0x50 is the I2c address, 0xF8 is the memory address where the read-only MAC value is
}

static void print_EEPROM_MAC_and_firmware(int deviceaddress, byte eeaddress)
{
  Wire.beginTransmission(deviceaddress);
  Wire.write(eeaddress); // LSB 
  Wire.endTransmission();
  Wire.requestFrom(deviceaddress, 8); //request 8 bytes from the device

  Serial.print("{");
  pr_field("device_id"); pr_qt();
  while (Wire.available()){
    
    Serial.print("0x");
    Serial.print(Wire.read(), HEX);
    Serial.print(" ");
  }
  pr_qt(); pr_sep();
  pr_field("firmware_id"); pr_qt();
  Serial.print(FIRMWARE_VERSION); pr_qt();
  Serial.print("}");
}
