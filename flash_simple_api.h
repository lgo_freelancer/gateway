#ifndef FLASH_SIMPLE_API
#define FLASH_SIMPLE_API

#include <stdint.h>
#include "config_structs.h"



enum {CFG_OK = 0, CFG_DOESNT_EXIST, CFG_EXISTS_ALREADY, CFG_NO_SPACE, CFG_WRITE_ERR, CFG_OPEN_ERR, CFG_READ_ERR};
enum {BKP_OK = 0, BKP_DOESNT_EXIST, BKP_EXISTS_ALREADY, BKP_NO_SPACE, BKP_WRITE_ERR, BKP_OPEN_ERR, BKP_READ_ERR, BKP_DELETE_ERR};

uint8_t flash_init(void);
uint8_t flash_ready(void);
void flash_wakeup(void);
void flash_erase_all(void);
void flash_print_all_files(void);


uint8_t gateway_config_file_create(Gateway_Cfg * cfg_str);
uint8_t gateway_config_file_read(Gateway_Cfg * cfg_str);
uint8_t gateway_config_file_update(Gateway_Cfg * cfg_str);
uint8_t gateway_config_check(void);

uint8_t backup_file_create(char * name, Sensor_Data * bkp_str);
uint8_t backup_file_read(char * name, Sensor_Data * bkp_str);
uint8_t backup_file_delete(char * name);

uint8_t gateway_node_config_update(Node_Gateway_Cfg * node_cfg);
uint8_t gateway_node_config_read(uint8_t node_id, Node_Gateway_Cfg * cfg_str);

uint8_t node_internal_config_update(Node_Internal_Cfg * node_cfg);
uint8_t node_internal_config_create(Node_Internal_Cfg * node_cfg);
uint8_t node_internal_config_read(Node_Internal_Cfg* cfg_str);


#endif
