#ifndef _COMM_H
#define _COMM_H

#include "config_structs.h"
#include <stdint.h>

enum rf_res_type {NOTHING, SENSOR_DATA, CONFIG_REQUEST, AUTH_REQUEST, ERROR};

uint8_t rf_init(void);
void rf_set_frequency(float freq);
void rf_set_power(int8_t pwr);
void rf_set_address(uint8_t address);
int16_t rf_rssi(void);
void rf_sleep(void);
void rf_set_timeout(uint32_t to);

enum rf_res_type listen_for_node_data(Sensor_Data * sdata, uint8_t * from, Gateway_Cfg * gwc);

#endif // _COMM_H
