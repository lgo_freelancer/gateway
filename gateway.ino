#include <Arduino.h>
#include <SerialFlash.h>
#include "flash_simple_api.h"
#include "config_structs.h"
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>

#include "print_helpers.h"
#include "config.h"
#include "comm.h"
#include "bridge_mqtt.h"

/* Global Objects */
Gateway_Cfg my_config;

/* it will check if it has received the byte 0xA2 for GW config. or 0xA1 for node config. Then the gateway will send the value 0xAA to mark that is ready to receive the config data.
   That way you can send config. structs whenever you want and the Arduino won't be blocked waiting innecesarily.
*/

/* Ele verificará se recebeu o byte 0xA2 para configuração GW. ou 0xA1 para a configuração do nó. Em seguida, o gateway enviará o valor 0xAA para marcar que está pronto
   para receber os dados de configuração.
    Dessa forma, você pode enviar a configuração. estrutura sempre que quiser e o Arduino não será bloqueado esperando incessantemente.
*/

enum {NOTHING_RECEIVED, GW_CONFIG_RECEIVED, NODE_CONFIG_RECEIVED};


uint8_t listen_for_serial_config(void);

void publish_node_data(Sensor_Data * node_data, uint16_t node_id, Adafruit_MQTT_Client &mqtt_client);

void setup(void) {

  /* Serial */
  Serial.begin(115200);

  /* Flash */

  /* Before starting flash, disable RF95 */
  /* Antes de iniciar o flash, desative RF95 */
  pinMode(RF95_CSS_PIN, OUTPUT);
  digitalWrite(RF95_CSS_PIN, HIGH);

  Serial.println(F("Starting Flash Memory.."));
  if (flash_init()) {
    Serial.println(F("Error flash memory communication!!!"));
    while (1); /* Trap, Laço*/
  }

  flash_wakeup();
  while (flash_ready());

  Serial.println(F("Starting Bridge"));
  bridge_init();
  Serial.println(F("Bridge iniciada"));
  /* Check if config exists in flash */
  /* Verifique se a configuração existe no flash */

  if (!gateway_config_check()) {
    /* Load gateway configuration */
    /* Usar Configuração existente no gateway */
    Serial.println(F("Using Existing Config."));
    gateway_config_file_read(&my_config);
  } else {
    Serial.println(F("Not Existing Config."));
    /* If not wait indefinitely for the Gateway Config */
    /* Se não esperar indefinidamente pela Configuração do Gateway */
    while (listen_for_serial_config() != GW_CONFIG_RECEIVED) {
    }
  }


  /* Radio Setup */
  Serial.println(F("Starting RF95.."));
  if (rf_init()) {
    Serial.println(F("Error RH_RF95 init failed"));
    while (1); /* Trap */
  }

  rf_set_frequency(915.0);

  Serial.println(F("Setup Done\n"));
  Serial.println(bridge_get_date());//Imprime data e hora do LinuxI
}

void loop(void) {
  /* Check Serial */
  /* Inicia Loop verificando a serial e enviando dados para Mqtt */
  
  Adafruit_MQTT_Client mqtt(bridge_get_client(), my_config.host_mqtt, my_config.port_mqtt, my_config.user_mqtt, my_config.pass_mqtt);

  rf_set_address(my_config.gateway_id);
  rf_set_timeout(1000);

  Sensor_Data rx_data;
  uint8_t from_node_id;
  enum rf_res_type res;

  while (1) {
    listen_for_serial_config();
    res = listen_for_node_data(&rx_data, &from_node_id, &my_config);
    if(res == SENSOR_DATA){
      mqtt_publish_node_data(&rx_data, from_node_id, &my_config, mqtt);
    }
    if((millis() - mqtt_last_ct())>300000){
      mqtt_ping(mqtt);
    }
  }
}


uint8_t listen_for_serial_config(void) {
  /* Check if there is serial data */
  /* 0xAE Config for Gateway */
  /* 0xA1 Config for Node */
  /* 0xA7 debbugging Flash */
  if (Serial.available()) {
    Serial.setTimeout(1000);
    uint8_t start_marker;
    start_marker = Serial.read();
    if (start_marker == 0xA1) {
      /* Config. GW */
      delay(50);
      Serial.write(0xAA);
      /* Wait for GW Config */
      /* Aguarde Configuração do GW */
      Gateway_Cfg gw_cfg;
      Serial.readBytes((char*)&gw_cfg, sizeof(Gateway_Cfg));
      delay(50);
      Serial.write(0xBB);
      /* Store in flash */
      /* Armazena na Flash */
      //gateway_config_file_create(&gw_cfg);
      gateway_config_file_update(&gw_cfg);

      my_config = gw_cfg;
      #if CRYPTO_ENABLED
      myCipher.setKey(my_config.crypto_key, sizeof(my_config.crypto_key));
      #endif
      Serial.println(F("Gateway Config Updated"));
      return GW_CONFIG_RECEIVED;
    } else if (start_marker == 0xA2) {
      /* Config. for Node */
      delay(50);
      Serial.write(0xAA);
      /* Wait for NODE Config */
      /* Aguarde Configuração do Node */
      Node_Gateway_Cfg node_cfg;
      Serial.readBytes((char*)&node_cfg, sizeof(Node_Gateway_Cfg));
      delay(50);
      Serial.write(0xBB);
      /* Store in flash */
      /* Armazena na Flash */
      gateway_node_config_update(&node_cfg);
      Serial.println(F("Node Config Updated"));
      return NODE_CONFIG_RECEIVED;
    } else if (start_marker == 0xA7) {
      /* Dump Contents of Flash. For debbugging purposes. */
      /* Imprime o conteudo da Flash para fins de depuração */
      flash_print_all_files();
      Serial.write(0xBB);
    } else if (start_marker == 0xA8) { /* Print the current GW Config */
      printer_gw_config(&my_config);
      Serial.write(0xBB);
    } else if (start_marker == 0xA9) {
      /* Erase Flash */
      flash_erase_all();
      Serial.write(0xBB);
    } else if (start_marker == 0xCA) { /* Get Node Configuration by ID */
      delay(50);
      Serial.write(0xAA);
      uint8_t node_id;
      node_id = Serial.read();
      Node_Gateway_Cfg tmp;
      uint8_t res = gateway_node_config_read(node_id, &tmp);
      if (res == CFG_OK) {
        printer_node_config(&tmp);
      }
      Serial.write(0xBB);
    } else if (start_marker == 0xCB) {
      printer_device_id();
      Serial.write(0xBB);
    } else {
      /* Flush if a not expected char is received */
      while (Serial.available()) {
        Serial.read();
      }
    }
  }
  return NOTHING_RECEIVED;
}
