#ifndef PRINT_HELPERS_H
#define PRINT_HELPERS_H

#include "config_structs.h"

void printer_gw_config(Gateway_Cfg * cfg);
void printer_node_config(Node_Gateway_Cfg * cfg);
void printer_device_id();

#endif
